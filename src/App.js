import React, { Component } from 'react';

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h2>Alistairs Javascript Universe</h2>
          <a href="/">Home</a>
          <a href="/hackernews">Hacker News</a>
          <a href="/countdown">Countdown</a>
        </div>
        <div>
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default App;