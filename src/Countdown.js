import React, {Component} from 'react';
import Clock from './Clock.js'

import './Countdown.css';
import { Form, FormControl, Button } from 'react-bootstrap';

class Countdown extends Component {
	constructor(props){
		super(props);
		this.state = {
			deadline: 'December 25, 2018',
			newDeadline: '',
		}
	}

	changeDeadline(){
		console.log('state', this.state);
		this.setState({deadline: this.state.newDeadline});
	}

	render() {
		return (
			<div className="Countdown">
				<div className="App-title"> Countdown to {this.state.deadline} </div>
				<Clock
					deadline = {this.state.deadline}
				/>
				<Form inline>
					<FormControl 
						className="Dealine-input"
						placeholder='new date' 
						onChange={event => this.setState({newDeadline: event.target.value})}
					/>
					<Button onClick={() => this.changeDeadline() }>Submit</Button>
				</Form>
			</div>
		)
	}
}

export default Countdown;