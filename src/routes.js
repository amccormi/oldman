import React from 'react';
import { Router, IndexRoute, Route } from 'react-router';

import App from './App';
import Home from './Home';
import Hacker from './Hacker';
import Countdown from './Countdown';


const Routes = (props) => (
  <Router {...props}>
    <Route path="/" component={App}>
      <IndexRoute component={Home} />
      <Route path="/home" component={Home} />
      <Route path="/hackernews" component={Hacker} />
      <Route path="/countdown" component={Countdown} />
    </Route>
  </Router>
);

export default Routes;